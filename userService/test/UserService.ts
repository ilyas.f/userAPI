var chai = require('chai');
var should = chai.should();
import { sequelize, models } from "../src/models/Index";
import { UserInstance, UserAttributes } from "../src/models/interfaces/UserInterface";
import { UserService } from "../src/services/UserService";

const delay = 500;

/**
 * Tests for the user service
 */
describe('User Service', () => {
    let userService: UserService;
    const userAttributes: UserAttributes = {
        email: "email@address.com",
        forename: "Joe",
        surname: "Bloggs"
    }
    before((done: Function) => {
        setTimeout(() => {
            sequelize.sync().then(() => {
                let service = require("../src/services/UserService");
                userService = service.userService;
                done();
            }).catch((error: Error) => {
                done(error);
            });
        }, delay);
    });

    describe("User Creation", () => {
        it('should create a user', () => {
            return models.users.create(userAttributes).then((user: UserInstance) => {
                user.dataValues.forename.should.equal(userAttributes.forename);
                user.dataValues.surname.should.equal(userAttributes.surname);
                user.dataValues.email.should.equal(userAttributes.email);
            }).catch((error: Error) => {
                throw error;
            });
        })
    })

    describe("User retrieval", () => {
        let userService: UserService
        let userId: number;
        before((done: Function) => {
            setTimeout(() => {
                sequelize.sync().then(() => {
                    let service = require("../src/services/UserService");
                    userService = service.userService;
                    return models.users.create(userAttributes).then((user: UserInstance) => {
                        userId = user.dataValues.id;
                        done();
                    })
                }).catch((error: Error) => {
                    done(error);
                });
            }, delay);
        });

        it('should retrieve all users', () => {
            return userService.retrieve().then((users: UserInstance[]) => {
                users.length.should.be.greaterThan(0);
            })
        });
        it('should retrieve a single user', () => {
            return userService.retrieveById(userId).then((user: UserInstance) => {
                user.dataValues.id.should.equal(userId);
            })
        });
    })


    describe("User deletion", () => {
        let userService: UserService;
        let userId: number;
        before((done: Function) => {
            setTimeout(() => {
                sequelize.sync().then(() => {
                    let service = require("../src/services/UserService");
                    userService = service.userService;
                    return models.users.create(userAttributes).then((user: UserInstance) => {
                        userId = user.dataValues.id;
                        done();
                    });
                }).catch((error: Error) => {
                    done(error);
                });
            }, delay);
        });
    })

    describe("User update", () => {
        let userService: UserService;
        let userId: number;
        before((done: Function) => {
            setTimeout(() => {
                sequelize.sync().then(() => {

                    let service = require("../src/services/UserService");
                    userService = service.userService;
                    return models.users.create(userAttributes).then((user: UserInstance) => {
                        userId = user.dataValues.id;
                        done();
                    });
                }).catch((error: Error) => {
                    done(error);
                });
            }, delay);
        });

        it("should update a user", () => {
            const updatedUserAttributes: UserAttributes = {
                email: "email@address.com",
                forename: "Joe",
                surname: "Bloggs"
            }
            return userService.update(userId, updatedUserAttributes).then((user: UserInstance) => {
                user.dataValues.forename.should.equal(updatedUserAttributes.forename);
                user.dataValues.surname.should.equal(updatedUserAttributes.surname);
                user.dataValues.email.should.equal(updatedUserAttributes.email);
            })
        })
    })

})