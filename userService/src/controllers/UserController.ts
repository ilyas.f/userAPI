import { Request, Response } from 'express';
import { userService } from "../services/UserService";
import { UserInstance } from "../models/interfaces/UserInterface";

/**
 * User Controller receives all user requests and forwards </br>
 * them to the User Service.
 * @class
 * @author Farooq Ilyas
 */
export class UserController {

    /**
     * Forwards a request to create a user
     * @param {Request} req request
     * @param {Response} res response
     * @param {Function} next next request handler
     * @returns {UserInstance} upon successful creation
     * @returns {Error} upon an error
     */
    create(req: Request, res: Response, next: Function) {
        userService.create(req.body)
            .then((user: UserInstance) => {
                return res.send(user);
            }).catch((error: Error) => {
                next(error);
            });
    }

    /**
     * Forwards a request to retrieve all users
     * @param {Request} req request
     * @param {Response} res response
     * @param {Function} next next request handler
     * @returns {UserInstance[]} upon successful retrieval
     * @returns {Error} upon an error
     */
    retrieve(req: Request, res: Response, next: Function) {
        userService.retrieve().
            then((users: UserInstance[]) => {
                if (users.length)
                    return res.send(users);
                next(new Error("NO_DATA"))
            }).catch((error: Error) => {
                next(error);
            });

    }

    /**
     * Forwards a request to retrieve a user given the userId
     * @param {Request} req request
     * @param {Response} res response
     * @param {Function} next next request handler 
     * @returns {UserInstance} upon successful retrieval
     * @returns {Error} upon error
     */
    retrieveById(req: Request, res: Response, next: Function) {
        userService.retrieveById(req.params.userID)
            .then((user: UserInstance) => {
                return res.send(user);
            }).catch((error: Error) => {
                next(error);
            });
    }

    /**
     * Forwards a request to delete a user given the id
     * @param {Request} req request 
     * @param {Response} res response 
     * @param {Function} next next request handler
     * @returns {number} number of rows deleted
     * @returns {Error} upon an error 
     */
    delete(req: Request, res: Response, next: Function) {
        userService.delete(req.params.userID)
            .then((deletedRows: number) => {
                return res.send({deleted:deletedRows});
            }).catch((error: Error) => {
                next(error);
            });
    }

    /**
     * Forwards a request to update a user given the id
     * @param {Request} req request 
     * @param {Response} res response 
     * @param {Function} next next request handler
     * @returns {UserInstance} upon successful update
     * @returns {Error} upon an error 
     */
    update(req: Request, res: Response, next: Function) {
        userService.update(req.params.userID, req.body)
            .then((user: UserInstance) => {
                return res.send(user);
            }).catch((error: Error) => {
                next(error);
            });
    }
}