import * as express from "express";
import * as http from "http";
import { configs } from "./config/Config";
import { logger, skip, stream } from "./utils/Logger";
import { Middleware } from './routers/Middleware';
import { BaseRouter } from './routers/BaseRouter';
import { sequelize } from "./models/Index";
import { Express, Request, Response } from "express";
import { CustomErrorHandler } from './ErrorHandler';

/**
 * Defines the contract for a server address
 * @interface
 * @author Farooq Ilyas
 */
interface ServerAddress {
  address: string;
  port: number;
  addressType: string;
}

/**
 * Initialises the server configuration - Setup of middleware 
 * and routes.
 * @class
 * @author Farooq Ilyas
 */
class Server {
  private app: Express;
  private server: http.Server;

  /**
   * @constructor
   */
  constructor() {
    this.app = express();
    this.app.use(new Middleware().configuration);
    this.app.use("/", new BaseRouter().routes);
    this.app.use("/", new CustomErrorHandler().error)
    this.server = http.createServer(this.app);
  }

  /**
   * Handles and throws errors
   * @param {NodeJS.ErrnoException} error error
   */
  private onError(error: NodeJS.ErrnoException): void {
    if (error.syscall) {
      logger.error(error)
      throw error;
    }

    let port = configs.getServerConfig().port;
    let bind = `Port ${port}`;

    switch (error.code) {
      case "EACCES":
        logger.error(`[EACCES] ${bind} requires elevated privileges.`);
        process.exit(1);
        break;
      case "EADDRINUSE":
        logger.error(`[EADDRINUSE] ${bind} is already in use.`);
        process.exit(1);
        break;
      default:
        throw error;
    }
  };

  /**
   * Logs the port information when a server listens
   */
  private onListening(): void {
    console.log("in on listening")
    let address = this.server.address();
    let bind = `port ${configs.getServerConfig().port}`;
    logger.info(`Listening on ${bind}.`);
  };

  /**
   * Starts the server and synchronises the database.
   */
  start(): void {
    sequelize.sync().then(() => {
      logger.info("Database synced.");
    }).catch((error: Error) => {
      logger.error(error.message);
    });
    this.server.listen(80);
    this.server.on("error", error => this.onError(error));
    this.server.on("listening", () => this.onListening());
  }

  /**
   * Stops the server.
   */
  stop(): void {
    this.server.close();
    process.exit(0);
  }
}

let server = new Server();
server.start();
process.on("SIGINT", () => {
  server.stop();
});
