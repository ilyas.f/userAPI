import { Express, Request, Response } from "express";
import * as express from "express";
var cors = require('cors');
//import * as cors from 'cors';
import * as compression from "compression";
import * as bodyParser from "body-parser";
import * as path from "path";
import * as morgan from "morgan";
import { logger, skip, stream } from "../utils/Logger";

/**
 * Intercepts all requests to perform middleware functions
 * @class
 * @author Farooq Ilyas
 */
export class Middleware {
    private app: Express;

    /**
     * Enables Cors </br>
     * Enables compression </br>
     * Enables bodyParser to encode request bodies </br>
     * Enables morgan for logging </br>
     * Enables an error handler (will eventually use custom error handler)
     * @constructor
     */
    constructor() {
        this.app = express();
        this.app.use(cors());
        this.app.use(compression());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.app.use(morgan("combined", { skip: skip, stream: <any>stream }));
    }

    /**
     * Returns the express app after configuration
     * @returns {Express} Express app
     */
    get configuration(): any {
        return this.app;
    }
}