import { Router, Request, Response } from 'express';
import { UserRouter } from '../routers/UserRouter';
var router = Router();

/**
 * Receives all requests and routes them accordingly
 * @class
 * @author Farooq Ilyas
 */
export class BaseRouter {

    /**
     * Returns the router after routes have been attached
     * @returns {Router} router
     */
    get routes() {
        router.use("/", (req: Request, res: Response, next: Function) => {
            if (req.get("id") == "1" && req.get("key") == "abracadabra") {
                next();
            }
            else {
                throw new Error("UNAUTHORISED");
            }
        })
        router.use("/users", new UserRouter().routes);
        return router;
    }
}