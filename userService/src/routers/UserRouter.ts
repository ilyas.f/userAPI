import { Router } from 'express';
import { UserController } from '../controllers/UserController';
import { validator } from '../validation/Validation';
var router = Router();

/**
 * Receives all user requests and routes them accordingly
 * @class
 * @author Farooq Ilyas
 */
export class UserRouter {

    /**
     * Returns the user router after the routes have been attached
     * @returns {Router} user router
     */
    get routes() {

        const userController: UserController = new UserController();

        router.route('/')
            .post(validator.validateUser, userController.create)
            .get(userController.retrieve);

        router.route("/:userID(\\d+)/")
            .get(userController.retrieveById)
            .delete(userController.delete)
            .put(validator.validateUser, userController.update);

        return router;
    }
}