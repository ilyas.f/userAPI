import * as cluster from "cluster";
import * as mkdirp from "mkdirp";
import * as path from "path";
import { configs } from "../config/Config";
import { transports, Logger } from "winston";
import { Request, Response } from "express";
require('winston-daily-rotate-file');

let config = configs.getLoggingConfig();
let dbConfig = configs.getDatabaseLoggingConfig();
config.file.filename = `/logs/${config.file.filename}`;
dbConfig.file.filename = `/logs/${dbConfig.file.filename}`;

if (cluster.isMaster) {
  mkdirp.sync(path.join(config.directory, `../${process.env.SERVICE}/logs`));
}

export const logger = new Logger({
  transports: [
    new transports.DailyRotateFile(config.file),
    new transports.Console(config.console),
  ],
  exitOnError: false
});

export const dbLogger = new Logger({
  transports: [
    new transports.DailyRotateFile(dbConfig.file),
    new transports.Console(dbConfig.console),
  ],
  exitOnError: false
});

export const skip = (req: Request, res: Response): boolean => {
  return res.statusCode >= 200;
};

export const stream = {
  write: (message: string, encoding: string): void => {
    logger.info(message);
  }
};

export const dbStream = {
  write: (message: string, encoding: string): void => {
    dbLogger.info(message);
  }
};
