import { UserAttributes, UserInstance } from '../models/interfaces/UserInterface';
import { logger } from "../utils/Logger";
import { models, sequelize } from "../models/Index";

/**
 * User service layer facilitates requests from the controller </br>
 * to the database
 * @class
 * @author Farooq Ilyas
 */
export class UserService {

    /**
     * Queries the database to create a user
     * @param {UserAttributes} user user attributes
     * @returns {UserInstance} user
     */
    async create(user: UserAttributes): Promise<UserInstance> {
        try {
            let userRecord = await models.users.create(user);
            logger.info(`Created user with id ${userRecord.dataValues.id}`);
            return userRecord;
        }
        catch (error) {
            throw error;
        }
    }

    /**
     * Queries the database to retrieve all users
     * @returns {UserInstance[]} all users
     */
    async  retrieve(): Promise<UserInstance[]> {
        try {
            let userRecords = await models.users.findAll();
            logger.info(`Retrieved all users`);
            return userRecords;
        } catch (error) {
            throw error;
        }
    }

    /**
     * Queries the database to retrieve a user given the id
     * @param {number} userId user id
     * @returns {UserInstance} upon successful retrieval
     * @returns {JSON} if user id not found
     */
    retrieveById(userId: number): Promise<UserInstance | { user: string }> {
        return new Promise<UserInstance | { user: string }>((resolve, reject) => {
            return models.users.findOne({ where: { id: userId } })
                .then((user: UserInstance) => {
                    if (user) {
                        logger.info(`Retrieved user with id ${userId}`);
                        resolve(user);
                    }
                    else {
                        logger.info(`User with id ${userId} does not exist`);
                        reject(new Error("NO_DATA"));
                    }
                });
        });
    }

    /**
     * Queries the database to delete a user given the id
     * @param {number} userId userId
     * @returns {number} rows deleted
     * @returns {JSON} user if id not found
     */
    delete(userId: number): Promise<number | { user: string }> {
        return new Promise<number | { user: string }>((resolve, reject) => {
            return models.users.destroy({ where: { id: userId } })
                .then((deletedRows: number) => {
                    if (deletedRows > 0) {
                        logger.info(`Deleted user with id ${userId}`);
                        resolve(deletedRows);
                    }
                    else {
                        logger.info(`User with id ${userId} does not exist`);
                        reject(new Error("NO_DATA"))
                    }
                });
        });
    }

    /**
     * Queries the database to update a user given the id
     * @param {number} userId user id 
     * @param {UserAttributes} userAttributes user attributes
     * @returns {UserInstance} updated user 
     */
    update(userId: number, userAttributes: UserAttributes): Promise<UserInstance> {
        return new Promise<UserInstance>((resolve, reject) => {
            return models.users.update(userAttributes, { where: { id: userId }, returning: true })
                .then((update: [number, UserInstance[]]) => {
                    resolve(models.users.findOne({ where: { id: userId } }));
                });
        });
    }
}
export const userService = new UserService();
