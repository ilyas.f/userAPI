import * as fs from "fs";
import * as path from "path";
import * as SequelizeStatic from "sequelize";
import { configs } from "../config/Config";
import { DBConfig, DatabaseConfig } from "../config/DatabaseConfig";
import { logger } from "../utils/Logger";
import { UserAttributes, UserInstance } from "./interfaces/UserInterface";
import { Sequelize } from "sequelize";

/**
 * Defines the contract for vehicle models
 * @interface
 * @author Farooq Ilyas
 */
export interface SequelizeModels {
  users: SequelizeStatic.Model<UserInstance, UserAttributes>;
}

/**
 * Configures the database according to the provided configuration. </br>
 * Imports the models and associations.
 * @class
 * @author Farooq Ilyas
 */
class Database {
  private basename: string;
  private models: SequelizeModels;
  private sequelize: Sequelize;

  /**
   * @constructor
   */
  constructor() {
    this.basename = path.basename(module.filename);
    let dbConfig = configs.getDatabaseConfig();
    this.setLogging(dbConfig);
    this.configureUserDatabase(dbConfig);
    this.importUserModels();
  }

  /**
   * Sets the level of logging for the database as per the configuration.
   * @param {DatabaseConfig} dbConfig database configuration
   */
  setLogging(dbConfig: DatabaseConfig) {
    if (dbConfig.logging) {
      dbConfig.logging = logger.info;
    }
  }

  /**
   * Creates the database connection
   * @param {DatabaseConfig} dbConfig database configuration
   */
  configureUserDatabase(dbConfig: DatabaseConfig) {
    this.sequelize = new SequelizeStatic('sqlite:///:memory:');
    this.models = ({} as any);
  }



  /**
   * Imports the models and their associations and 
   * associates them with the connection
   */
  importUserModels() {
    fs.readdirSync(__dirname).filter((file: string) => {
      return (file !== this.basename) && (file !== "interfaces");
    }).forEach((file: string) => {
      let model = this.sequelize.import(path.join(__dirname, file));
      this.models[(model as any).name] = model;
    });


    Object.keys(this.models).forEach((modelName: string) => {
      if (typeof this.models[modelName].associate === "function") {
        this.models[modelName].associate(this.models);
      }
    });
  }

  /**
   * Returns the models
   * @returns {SequelizeModels} models
   */
  getModels() {
    return this.models;
  }

  /**
   * Returns the database connection
   * @returns {SequelizeStatic.Sequelize} database connection
   */
  getSequelize() {
    return this.sequelize;
  }

}

const database = new Database();
export const models = database.getModels();
export const sequelize = database.getSequelize();
