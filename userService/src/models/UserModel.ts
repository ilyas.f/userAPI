import * as SequelizeStatic from "sequelize";
import { DataTypes, Sequelize } from "sequelize";
import { UserAttributes, UserInstance } from "./interfaces/UserInterface";

/**
 * Defines the User model and any associations
 * @class
 * @author Farooq Ilyas
 */
class UserModel {
    private user: SequelizeStatic.Model<UserInstance, UserAttributes>;
    constructor(sequelize: Sequelize, dataTypes: DataTypes) {
        this.user = sequelize.define<UserInstance, UserAttributes>("users", {
            id: {
                type: dataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            email: {
                type: dataTypes.TEXT,
                allowNull: false
            },
            forename: {
                type: dataTypes.TEXT,
                allowNull: false
            },
            surname: {
                type: dataTypes.TEXT,
                allowNull: false
            }

        }, {
                indexes: [],
                classMethods: {
                    associate: (models) => { }
                },
                timestamps: true,
            });
    }

    /**
     * Returns the user model
     * @returns {SequelizeStatic.Model<UserInstance,UserAttributes>} model
     */
    get(): SequelizeStatic.Model<UserInstance, UserAttributes> {
        return this.user;
    }
}
export default (sequelize: Sequelize, dataTypes: DataTypes) => {
    return new UserModel(sequelize, dataTypes).get();
}