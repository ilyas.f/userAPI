import { Instance } from "sequelize";

/**
 * Defines the contract for a user
 * @interface
 */
export interface UserAttributes {
    id?: number;
    email: string;
    forename: string;
    surname: string;
    createdAt?: string;
    updatedAt?: string;
}

/**
 * Defines the contract for an instance of a user
 * @interface
 */
export interface UserInstance extends Instance<UserAttributes> {
    dataValues: UserAttributes;
}