import { UserAttributes } from "../models/interfaces/UserInterface";
import { Request, Response } from 'express';
class Validator {

    validateUser(req: Request, res: Response, next: Function) {
        let user: UserAttributes = req.body;

        if (!user.forename)
            throw new Error("MISSING_FORENAME");
        if (!user.forename.match(/^[A-Za-z]+$/))
            throw new Error("INVALID_FORENAME_FORMAT");

        if (!user.surname)
            throw new Error("MISSING_SURNAME");
        if (!user.surname.match(/^[A-Za-z]+$/))
            throw new Error("INVALID_SURNAME_FORMAT");

        if (!user.email)
            throw new Error("MISSING_EMAIL");
        if (!user.email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/))
            throw new Error("INVALID_EMAIL_FORMAT")

        next();
    }
}

export const validator = new Validator();