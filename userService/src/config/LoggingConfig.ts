/**
 * @interface
 * Defines the contract for a logging configuration
 * @author Farooq Ilyas
 */
export interface LoggingConfig {
  file: {
    level: string,
    filename: string,
    handleExceptions: boolean,
    json: boolean,
    maxsize: number,
    maxFiles: number,
    colorize: boolean,
    prepend: boolean,
    datePattern: string
  };
  console: {
    level: string,
    handleExceptions: boolean,
    json: boolean,
    colorize: boolean
  };
  directory: string;
}

/**
 * Provides the configuration for logging
 * @class
 * @author Farooq Ilyas
 */
class LoggingConfiguration {
  private loggingConfig: LoggingConfig;
  private databaseLoggingConfig: LoggingConfig;

  /**
   * @constructor
   */
  constructor() {
    this.loggingConfig = {
      file: {
        level: "info",
        filename: `${process.env.SERVICE}.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880,
        maxFiles: 100,
        colorize: true,
        prepend: true,
        datePattern: 'yyyy-MM-dd.'
      },
      console: {
        level: (process.env.NODE_ENV == "development") ? "info" : "info",
        handleExceptions: true,
        json: false,
        colorize: true
      },
      directory: __dirname

    };

    this.databaseLoggingConfig = {
      file: {
        level: "info",
        filename: `${process.env.SERVICE}-database.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880,
        maxFiles: 100,
        colorize: true,
        prepend: true,
        datePattern: 'yyyy-MM-dd.'
      },
      console: {
        level: (process.env.NODE_ENV == "development") ? "info" : "none",
        handleExceptions: true,
        json: false,
        colorize: true
      },
      directory: __dirname

    };
  }

  /**
   * Returns the logging configuration
   * @returns {LoggingConfig} JSON logging configuration
   */
  getLoggingConfig() {
    return this.loggingConfig;
  }

  getDbLoggingConfig() {
    return this.databaseLoggingConfig;
  }
}
export const loggingConfig = new LoggingConfiguration();


