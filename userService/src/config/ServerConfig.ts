/**
 * @interface
 * Defines the contract for a server configuration
 * @author Farooq Ilyas
 */
export interface ServerConfig {
  port: number;
  session: {
    name: string,
  };
}

/**
 * Provides the configuration for the server
 * @class
 * @author Farooq Ilyas
 */
class ServerConfiguration {
  private serverConfig: ServerConfig;

  constructor() {
    this.serverConfig = {
      port: 80,
      session: {
        name: "user-service",
      }
    };
  }

  /**
   * Returns the server configuration.
   * @returns {ServerConfig} JSON server configuration
   */
  getServerConfig() {
    return this.serverConfig;
  }
}
export const serverConfig = new ServerConfiguration(); 
