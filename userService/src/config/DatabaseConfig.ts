/**
 * Defines the contract for a database configuration
 * @interface
 * @author Farooq Ilyas
 */
export interface DatabaseConfig {
    username: string;
    password: string;
    database: string;
    host: string;
    port: number;
    dialect: string;
    logging: boolean | Function;
    force: boolean;
    timezone?: string;
}

/**
 * Provides the database configuration
 * @class
 * @author Farooq Ilyas
 */
export class DBConfig {
    private databaseConfig: DatabaseConfig;

    /**
     * @constructor
     */
    constructor() {
        this.databaseConfig = {
            username: "username",
            password: "password",
            database: "database",
            host: "localhost",
            port: 5432,
            dialect: "postgres",
            logging: true,
            //force set to true will drop and recreate tables
            force: true,
            timezone: "+00:00"
        }
    }

    /**
     * Returns the database configuration
     * @returns {DatabaseConfig} JSON database configuration
     */
    DatabaseConfig() {
        return this.databaseConfig;
    }
}