import { DBConfig, DatabaseConfig } from "./DatabaseConfig";
import { loggingConfig, LoggingConfig, } from "./LoggingConfig";
import { serverConfig, ServerConfig } from "./ServerConfig";

/**
 * Consolidates the configuration for the server, database and logging.
 * @class
 * @author Farooq Ilyas
 */
class Configs {
    private loggingConfig: LoggingConfig;
    private databaseLoggingConfig: LoggingConfig;
    private serverConfig: ServerConfig;
    private dbConfig: DBConfig;

    /**
     * @constructor
     */
    constructor() {
        this.dbConfig = new DBConfig();
        this.loggingConfig = loggingConfig.getLoggingConfig();
        this.databaseLoggingConfig = loggingConfig.getDbLoggingConfig();
        this.serverConfig = serverConfig.getServerConfig();
    }

    /**
     * Returns the database configuration
     * @returns {DatabaseConfig} JSON database configuration
     */
    getDatabaseConfig(): DatabaseConfig {
        return this.dbConfig.DatabaseConfig();
    }

    /**
     * Returns the configuration for logging
     * @returns{LoggingConfig} JSON logging configuration
     */
    getLoggingConfig(): LoggingConfig {
        return this.loggingConfig;
    }


    /**
     * Returns the configuration for database logging
     * @returns{LoggingConfig} JSON logging configuration
     */
    getDatabaseLoggingConfig(): LoggingConfig {
        return this.databaseLoggingConfig;
    }
    /**
     * Returns the server configuration
     * @returns {ServerConfig} JSON server configuration
     */
    getServerConfig(): ServerConfig {
        return this.serverConfig;
    }
}

export const configs = new Configs();
