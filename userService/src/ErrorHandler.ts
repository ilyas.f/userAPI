import { Request, Response } from 'express';
import { logger } from "./utils/Logger";
/**
 * CustomErrorHandler handles all errors thrown in the application
 * @class
 * @author Farooq Ilyas
 */
export class CustomErrorHandler {

    private errorCodes = {
        "NO_DATA": { "code": 404, message: "NO DATA FOUND" },
        "MISSING_FORENAME": { code: 400, message: "Forename must be provided" },
        "MISSING_SURNAME": { code: 400, message: "Surname must be provided" },
        "MISSING_EMAIL": { code: 400, message: "Email address must be provided" },
        "INVALID_EMAIL_FORMAT": { code: 400, message: "Invalid email address format - check entry" },
        "INVALID_FORENAME_FORMAT": { code: 400, message: "Invalid forename format - check entry" },
        "INVALID_SURNAME_FORMAT": { code: 400, message: "Invalid surname format - check entry" },
        "UNAUTHORISED": { code: 401, message: "You are not authorised for this operation" }
    }

    /**
     * Returns the relevant error message
     * @returns {JSON} error error message
     */
    error = (err: Error, req: Request, res: Response, next: Function) => {
        if (err.message && this.errorCodes[err.message]) {
            logger.error(this.errorCodes[err.message].message);
            res.status(this.errorCodes[err.message].code || 500).send({ error: this.errorCodes[err.message].message || err })
        } else {
            logger.error(err)
            res.status(500).send(err);
        }

    }
}
