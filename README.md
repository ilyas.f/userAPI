# User Service API

## Setup
Out of the box, the application uses an sqlite in memory database.  
Database configuration parameters for other databases should be provided in `configs/DatabaseConfig.ts`.

#### Running the application:  

First install dependencies:
```
npm install
```
Compile the project: 
```
tsc
```
Start the application:
```
npm start
```
This will run the application from `build/src`.

## Test
To run the tests:
```
npm test
```

## Endpoint URLs
POST  
```
/users
```
GET
```
/users
/users/<userid>
```
PUT
```
/users/<userid>
```
#### Running in Docker (assuming docker is installed)
* Navigate to root of the project
* Run command: `docker-compose up`